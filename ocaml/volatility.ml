(* volatility.ml                                                           *)
(* Copyright (C) 2013 Carl Pulley <c.j.pulley@hud.ac.uk>                   *)
(*                                                                         *)
(* This program is free software; you can redistribute it and/or modify    *)
(* it under the terms of the GNU General Public License as published by    *)
(* the Free Software Foundation; either version 2 of the License, or (at   *)
(* your option) any later version.                                         *)
(*                                                                         *)
(* This program is distributed in the hope that it will be useful, but     *)
(* WITHOUT ANY WARRANTY; without even the implied warranty of              *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        *)
(* General Public License for more details.                                *)
(*                                                                         *)
(* You should have received a copy of the GNU General Public License       *)
(* along with this program; if not, write to the Free Software             *)
(* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA *)

open Unsigned

(* Exception wrapping up Volatility/Python exceptions *)
exception VolatilityException of string
let _ = Callback.register_exception "VolatilityException" (VolatilityException "any string")

(* Abstract type which represents a Volatility object instance *)
type vol_obj

type py_object =
  PyNone
  | PyInt of UInt64.t
  | PyBool of bool
  | PyFloat of float
  | PyString of string
  | PyTuple of py_object array
  | PyList of py_object list
  | PyDict of (py_object * py_object) list
  | PyObject of vol_obj

type py_eval =
  PyAttribute of string
  | PyMethod of string * py_object * py_object
  | PyCall of py_object * py_object
  | PyIter of py_object BatLazyList.t

let empty_tuple = PyTuple [||]

let empty_dict = PyDict []

external _VolatilityObject_callback: vol_obj -> py_eval list -> py_object = "VolatilityObject_callback"

(* Allows Python iteration objects to be lazily unwound *)
let py_iter as_PyType iter_obj = BatLazyList.from (fun () ->
  try
    as_PyType(_VolatilityObject_callback iter_obj [ PyMethod( "next", empty_tuple, empty_dict ) ])
  with VolatilityException "<type 'exceptions.StopIteration'>" -> 
    raise BatLazyList.No_more_elements
)

let py_object_to_string = function
| PyNone -> "PyNone"
| PyInt _ -> "PyInt"
| PyBool _ -> "PyBool"
| PyFloat _ -> "PyFloat"
| PyString _ -> "PyString"
| PyTuple _ -> "PyTuple"
| PyList _ -> "PyList"
| PyDict _ -> "PyDict"
| PyObject _ -> "PyObject"

let as_PyInt = function
| PyInt n -> n
| pyobj -> raise (VolatilityException ("as_PyInt: got a " ^ (py_object_to_string pyobj) ^ " [" ^ (Batteries.dump pyobj) ^ "], expected a PyInt type"))

let as_PyBool = function
| PyInt n -> n <> UInt64.zero
| pyobj -> raise (VolatilityException ("as_PyBool: got a " ^ (py_object_to_string pyobj) ^ " [" ^ (Batteries.dump pyobj) ^ "], expected a PyBool type"))

let as_PyFloat = function
| PyFloat n -> n
| pyobj -> raise (VolatilityException ("as_PyFloat: got a " ^ (py_object_to_string pyobj) ^ " [" ^ (Batteries.dump pyobj) ^ "], expected a PyFloat type"))

let as_PyString = function
| PyString n -> n
| pyobj -> raise (VolatilityException ("as_PyString: got a " ^ (py_object_to_string pyobj) ^ " [" ^ (Batteries.dump pyobj) ^ "], expected a PyString type"))

let as_PyTuple = function
| PyTuple n -> n
| pyobj -> raise (VolatilityException ("as_PyTuple: got a " ^ (py_object_to_string pyobj) ^ " [" ^ (Batteries.dump pyobj) ^ "], expected a PyTuple type"))

let as_PyList = function
| PyList n -> n
| pyobj -> raise (VolatilityException ("as_PyList: got a " ^ (py_object_to_string pyobj) ^ " [" ^ (Batteries.dump pyobj) ^ "], expected a PyList type"))

let as_PyDict = function
| PyDict n -> n
| pyobj -> raise (VolatilityException ("as_PyDict: got a " ^ (py_object_to_string pyobj) ^ " [" ^ (Batteries.dump pyobj) ^ "], expected a PyDict type"))

let as_PyObject = function
| PyObject n -> n
| pyobj -> raise (VolatilityException ("as_PyObject: got a " ^ (py_object_to_string pyobj) ^ " [" ^ (Batteries.dump pyobj) ^ "], expected a PyObject type"))

let as_Option py_type = function
| PyNone -> 
    None
| pyobj -> 
    try
      Some(py_type pyobj)
    with VolatilityException msg ->
      raise (VolatilityException ("as_Option: " ^ msg))
