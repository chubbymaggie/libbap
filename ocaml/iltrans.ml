(* iltrans.ml                                                              *)
(*                                                                         *)
(* This program is free software; you can redistribute it and/or modify    *)
(* it under the terms of the GNU General Public License as published by    *)
(* the Free Software Foundation; either version 2 of the License, or (at   *)
(* your option) any later version.                                         *)
(*                                                                         *)
(* This program is distributed in the hope that it will be useful, but     *)
(* WITHOUT ANY WARRANTY; without even the implied warranty of              *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        *)
(* General Public License for more details.                                *)
(*                                                                         *)
(* You should have received a copy of the GNU General Public License       *)
(* along with this program; if not, write to the Free Software             *)
(* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA *)

open Utils_common
open Volatility

type ast = Ast.program
type astcfg = Cfg.AST.G.t
type ssa = Cfg.SSA.G.t

type prog =
  | Ast of ast
  | AstCfg of astcfg
  | Ssa of ssa

type cmd =
  | AnalysisAst of (ast -> unit)
  | AnalysisAstCfg of (astcfg -> unit)
  | AnalysisSsa of (ssa -> unit)
  | TransformAst of (ast -> ast)
  | TransformAstCfg of (astcfg -> astcfg)
  | TransformSsa of (ssa -> ssa)
  | ToCfg of bool
  | ToAst of bool
  | ToSsa of bool
 (* add more *)

type state = { pipeline: cmd list; startdebug: int }

let default_state = { pipeline = []; startdebug = 1 }

let post_process_dot post_process filename = 
  if post_process then
    let _ = Sys.command ("xdot " ^ filename) in
      ()

type filter = [ `None | `Cdg | `PdG | `Ddg ]

let output_ast fopt p =
  let oc = match fopt with | None -> stdout | Some f -> open_out f in
  let close () = match fopt with | None -> () | Some _ -> close_out oc in
  let pp = new Pp.pp_oc oc in
    pp#ast_program p;
    pp#close;
    close()

let output_cfg ?(filter=`None) ?(stmts=true) ?(bbids=true) ?(asms=true) ?(linenums=true) ?(structanal=true) fopt cfg =
  let filename = match fopt with 
    | None -> Filename.temp_file "bap_" ".dot"
    | Some f -> f
  in
  let oc = open_out filename in
  let graph = match filter with
    | `None -> cfg
    | `Cdg -> Depgraphs.CDG_AST.compute_cdg cfg
    | `Pdg -> Depgraphs.PDG_AST.compute_pdg cfg
    | `Ddg -> Depgraphs.DDG_AST.compute_ddg cfg
  in
  let module Dot = Cfg_pp.CfgDot(struct let stmts=stmts;; let bbids=bbids;; let asms=asms;; let linenums=linenums;; let structanal=structanal end) in
    Dot.output_graph oc graph;
    close_out oc;
    post_process_dot (BatOption.is_none fopt) filename

let output_ssa ?(filter=`None) ?(stmts=true) ?(bbids=true) ?(asms=true) ?(linenums=true) ?(structanal=true) fopt ssa=
  let filename = match fopt with 
    | None -> Filename.temp_file "bap_" ".dot"
    | Some f -> f
  in
  let oc = open_out filename in
  let graph = match filter with
    | `None -> ssa
    | `Cdg -> Depgraphs.CDG_SSA.compute_cdg ssa
    | `Pdg -> failwith "can not compute program depenence graphs (PDG) using static single assignment (SSA) graphs"
    | `Ddg -> Depgraphs.DDG_SSA.compute_ddg ssa
  in
  let module Dot = Cfg_pp.SsaDot(struct let stmts=stmts;; let bbids=bbids;; let asms=asms;; let linenums=linenums;; let structanal=structanal end) in
    Dot.output_graph oc graph;
    close_out oc;
    post_process_dot (BatOption.is_none fopt) filename

let output_c fopt p =
  let oc = match fopt with | None -> stdout | Some f -> open_out f in
  let close () = match fopt with | None -> () | Some _ -> close_out oc in
  let ft = Format.formatter_of_out_channel oc in
  let pp = new To_c.pp ft in
    pp#ast_program p;
    close()

let to_dsa p =
  let p,_ = Traces.to_dsa p in
  p

let output_structanal cfg =
  let module SA = Structural_analysis in
  let trimmed_cfg = Prune_unreachable.prune_unreachable_ast cfg in
  let sa = SA.structural_analysis trimmed_cfg in
  let region = ref 0 in
  let sa_map = ref Cfg.BM.empty in
  let rec walk_nodes ctxt_region ctxt_type = function
    | SA.BBlock bbid ->
        sa_map := Cfg.BM.add bbid (ctxt_region, ctxt_type) !sa_map
    | SA.Region(rt, l) -> 
        let region_type = SA.rtype2s rt in
          region := !region + 1;
          List.iter (walk_nodes (!region :: ctxt_region) region_type) l
  in
    walk_nodes [] "?" sa;
    Cfg.BM.fold (fun bbid (ctxt_region, ctxt_type) g -> 
      let node = Cfg.AST.find_vertex g bbid in
      let stmts = Cfg.AST.get_stmts g node in
      let attr = Type.ExnAttr(Type.Struct_ctxt(ctxt_region, ctxt_type)) in
      let new_stmts =
        match stmts with
        | Ast.Comment("", attrs) :: stmts ->
            Ast.Comment("", attr :: attrs) :: stmts
        | _ ->
            Ast.Comment("", [attr]) :: stmts
      in
        Cfg.AST.set_stmts g node new_stmts
    ) !sa_map cfg

let sccvn p =
  fst(Sccvn.replacer p)
let deadcode p =
  fst(Deadcode.do_dce p)
let adeadcode p =
  fst(Deadcode.do_aggressive_dce p)
let jumpelim p =
  fst(Ssa_simp_misc.cfg_jumpelim p)
let ast_coalesce = 
  Coalesce.coalesce_ast
let ssa_coalesce = 
  Coalesce.coalesce_ssa

let bbid_of_string = function
  | "BB_Entry" -> Cfg.BB_Entry
  | "BB_Exit" -> Cfg.BB_Exit
  | "BB_Indirect" -> Cfg.BB_Indirect
  | "BB_Error" -> Cfg.BB_Error
  | _ as bbid -> Cfg.BB(int_of_string(String.sub bbid 3 (String.length bbid - 3)))

let add_assertion bb bbN expr astcfg =
  let scope = Grammar_private_scope.default_scope() in
  let expr_term, _ = Parser.exp_from_string ~scope expr in
  let expr_type = Typecheck.infer_ast expr_term in
  let () = match expr_type with | Type.Reg 1 -> () | _ -> failwith "Assertion should be a boolean type" in
  let vertex = Cfg.AST.find_vertex astcfg (bbid_of_string bb) in
  let pre_stmts, post_stmts = BatList.split_at bbN (Cfg.AST.get_stmts astcfg vertex) in
  let new_astcfg = Cfg.AST.set_stmts astcfg vertex (List.concat [pre_stmts; [Ast.Assert(expr_term, [])]; post_stmts]) in
    new_astcfg

let add_assumption bb bbN expr astcfg =
  let scope = Grammar_private_scope.default_scope() in
  let expr_term, _ = Parser.exp_from_string ~scope expr in
  let expr_type = Typecheck.infer_ast expr_term in
  let () = match expr_type with | Type.Reg 1 -> () | _ -> failwith "Assumption should be a boolean type" in
  let vertex = Cfg.AST.find_vertex astcfg (bbid_of_string bb) in
  let pre_stmts, post_stmts = BatList.split_at bbN (Cfg.AST.get_stmts astcfg vertex) in
  let new_astcfg = Cfg.AST.set_stmts astcfg vertex (List.concat [pre_stmts; [Ast.Assume(expr_term, [])]; post_stmts]) in
    new_astcfg

let ast_chop srcbb srcn trgbb trgn p =
  Ast_slice.CHOP_AST.chop p srcbb srcn trgbb trgn
let ssa_chop srcbb srcn trgbb trgn p =
  Ssa_slice.CHOP_SSA.chop p srcbb srcn trgbb trgn

let undef_vars cfg =
  let module DEFS = Depgraphs.DEFS_AST in
  let undef_vars, poss_undef_vars = DEFS.undefinedvars cfg in
  let poss_undef_vars = Var.VarSet.filter (fun v -> not(Var.VarSet.mem v undef_vars)) poss_undef_vars in
  let vars = DEFS.vars cfg in
  let defs = Var.VarSet.filter (fun v -> not(Var.VarSet.mem v undef_vars) && not(Var.VarSet.mem v poss_undef_vars)) vars in
  let entry = Cfg.AST.find_vertex cfg Cfg.BB_Entry in
  let entry_stmts = Cfg.AST.get_stmts cfg entry in
  let undef_attrs = Var.VarSet.fold (fun v l -> (Type.StrAttr("A undef(" ^ (Pp.var_to_string v) ^ ")")) :: l) undef_vars [] in
  let poss_undef_attrs = Var.VarSet.fold (fun v l -> (Type.StrAttr("E undef(" ^ (Pp.var_to_string v) ^ ")")) :: l) poss_undef_vars [] in
  let def_attrs = Var.VarSet.fold (fun v l -> (Type.StrAttr("A def(" ^ (Pp.var_to_string v) ^ ")")) :: l) defs [] in
    Cfg.AST.set_stmts cfg entry (Ast.Comment("Undefined", List.concat [undef_attrs; poss_undef_attrs]) :: Ast.Comment("Defined", def_attrs) :: entry_stmts)

let typ = 0
let cmd = 1
let argN n arg = arg.(2+n)

let process_arg state arg =
  let arg_size = Array.length(arg) in
    if arg_size >= 2 && arg.(typ) = "iltrans" then
      if arg.(cmd) = "-pp-ast" && arg_size <= 3 then
        let fopt = if arg_size = 2 then None else Some(argN 0 arg) in
          { state with pipeline = AnalysisAst(output_ast fopt) :: state.pipeline }
      else if arg.(cmd) = "-pp-cfg" && (arg_size = 8 || arg_size = 9) then
        let filter = match (argN 0 arg) with
          | "cdg" -> `Cdg
          | "pdg" -> `Pdg
          | "ddg" -> `Ddg
          | _ -> `None
        in
        let stmts = bool_of_string(argN 1 arg) in
        let bbids = bool_of_string(argN 2 arg) in
        let asms = bool_of_string(argN 3 arg) in
        let linenums = bool_of_string(argN 4 arg) in
        let structanal = bool_of_string(argN 5 arg) in
        let fopt = if arg_size = 8 then None else Some(argN 6 arg) in
          { state with pipeline = AnalysisAstCfg(output_cfg ~filter ~stmts ~bbids ~asms ~linenums ~structanal fopt) :: state.pipeline }
      else if arg.(cmd) = "-pp-ssa" && (arg_size = 8 || arg_size = 9) then
        let filter = match (argN 0 arg) with
          | "cdg" -> `Cdg
          | "pdg" -> `Pdg
          | "ddg" -> `Ddg
          | _ -> `None
        in
        let stmts = bool_of_string(argN 1 arg) in
        let bbids = bool_of_string(argN 2 arg) in
        let asms = bool_of_string(argN 3 arg) in
        let linenums = bool_of_string(argN 4 arg) in
        let structanal = bool_of_string(argN 5 arg) in
        let fopt = if arg_size = 8 then None else Some(argN 6 arg) in
          { state with pipeline = AnalysisSsa(output_ssa ~filter ~stmts ~bbids ~asms ~linenums ~structanal fopt) :: state.pipeline }
      else if arg.(cmd) = "-pp-varnums" && arg_size = 3 then
        let varnums = bool_of_string(argN 0 arg) in
          (Pp.output_varnums := varnums; state)
      else if arg.(cmd) = "-struct" && arg_size = 2 then
        { state with pipeline = TransformAstCfg(output_structanal) :: state.pipeline }
      else if arg.(cmd) = "-vsa" && arg_size = 2 then
        { state with pipeline = TransformSsa(Bap_analysis.vsa) :: state.pipeline }
      else if arg.(cmd) = "-assert" && arg_size = 5 then
        let expr = argN 0 arg in
        let bb = argN 1 arg in
        let bbN = int_of_string(argN 2 arg) in
          { state with pipeline = TransformAstCfg(add_assertion bb bbN expr) :: state.pipeline }
      else if arg.(cmd) = "-assume" && arg_size = 5 then
        let expr = argN 0 arg in
        let bb = argN 1 arg in
        let bbN = int_of_string(argN 2 arg) in
          { state with pipeline = TransformAstCfg(add_assumption bb bbN expr) :: state.pipeline }
      else if arg.(cmd) = "-undef-vars" && arg_size = 2 then
        { state with pipeline = TransformAstCfg(undef_vars) :: state.pipeline }
      else if arg.(cmd) = "-to-cfg" && arg_size = 3 then
        let special_error = bool_of_string(argN 0 arg) in
          { state with pipeline = ToCfg(special_error) :: state.pipeline }
      else if arg.(cmd) = "-to-ast" && arg_size = 3 then
        let special_error = bool_of_string(argN 0 arg) in
          { state with pipeline = ToAst(special_error) :: state.pipeline }
      else if arg.(cmd) = "-to-ssa" && arg_size = 3 then
        let special_error = bool_of_string(argN 0 arg) in
          { state with pipeline = ToSsa(special_error) :: state.pipeline }
      else if arg.(cmd) = "-to-c" && arg_size <= 3 then
        let fopt = if arg_size = 2 then None else Some(argN 0 arg) in
          { state with pipeline = AnalysisAst(output_c fopt) :: state.pipeline }
      else if arg.(cmd) = "-ast-chop" && arg_size = 6 then
        { state with pipeline = TransformAstCfg(ast_chop (int_of_string(argN 0 arg)) (int_of_string(argN 1 arg)) (int_of_string(argN 2 arg)) (int_of_string(argN 3 arg))) :: state.pipeline }
      else if arg.(cmd) = "-ssa-chop" && arg_size = 6 then
        { state with pipeline = TransformSsa(ssa_chop (int_of_string(argN 0 arg)) (int_of_string(argN 1 arg)) (int_of_string(argN 2 arg)) (int_of_string(argN 3 arg))) :: state.pipeline }
      else if arg.(cmd) = "-sccvn" && arg_size = 2 then
        { state with pipeline = (TransformSsa sccvn) :: state.pipeline }
      else if arg.(cmd) = "-deadcode" && arg_size = 2 then
        { state with pipeline = (TransformSsa deadcode) :: state.pipeline }
      else if arg.(cmd) = "-adeadcode" && arg_size = 2 then
        { state with pipeline = (TransformSsa adeadcode) :: state.pipeline }
      else if arg.(cmd) = "-coalesce-ast" && arg_size = 2 then
        { state with pipeline = (TransformAstCfg ast_coalesce) :: state.pipeline }
      else if arg.(cmd) = "-coalesce-ssa" && arg_size = 2 then
        { state with pipeline = (TransformSsa ssa_coalesce) :: state.pipeline }
      else if arg.(cmd) = "-jumpelim" && arg_size = 2 then
        { state with pipeline = (TransformSsa jumpelim) :: state.pipeline }
      else if arg.(cmd) = "-simp-ssa" && arg_size = 5 then
        let usesccvn = bool_of_string(argN 0 arg) in
        let usedc = bool_of_string(argN 1 arg) in
        let usemisc = bool_of_string(argN 2 arg) in
          { state with pipeline = (TransformSsa (Ssa_simp.simp_cfg ~liveout:Asmir.all_regs ~usesccvn ~usedc ~usemisc)) :: state.pipeline }
      else if arg.(cmd) = "-single-stmt-ssa" && arg_size = 2 then
        { state with pipeline = (TransformSsa Depgraphs.DDG_SSA.stmtlist_to_single_stmt) :: state.pipeline }
      else if arg.(cmd) = "-normalize-mem" && arg_size = 2 then
        { state with pipeline = (TransformAst Memory2array.coerce_prog) :: state.pipeline }
      else if arg.(cmd) = "-prune-cfg" && arg_size = 2 then
        { state with pipeline = (TransformAstCfg Prune_unreachable.prune_unreachable_ast) :: state.pipeline }
      else if arg.(cmd) = "-prune-ssa" && arg_size = 2 then
        { state with pipeline = (TransformSsa Prune_unreachable.prune_unreachable_ssa) :: state.pipeline }
      else if arg.(cmd) = "-unroll" && arg_size = 4 then
        let num = int_of_string(argN 0 arg) in
        let alg = argN 1 arg in
          if alg = "steensgard" then
            { state with pipeline = (TransformAstCfg(Unroll.unroll_loops_steensgard ~count:num)) :: state.pipeline }
          else if alg = "sa" then
            { state with pipeline = (TransformAstCfg(Unroll.unroll_loops_sa ~count:num)) :: state.pipeline }
          else
            failwith (alg ^ " is not a recognised loop unrolling algorithm")
      else if arg.(cmd) = "-rm-cycles" && arg_size = 2 then
        { state with pipeline = (TransformAstCfg Hacks.remove_cycles) :: state.pipeline }
      else if arg.(cmd) = "-rm-indirect-ast" && arg_size = 2 then
        { state with pipeline = (TransformAstCfg Hacks.ast_remove_indirect) :: state.pipeline }
      else if arg.(cmd) = "-rm-indirect-ssa" && arg_size = 2 then
        { state with pipeline = (TransformSsa Hacks.ssa_remove_indirect) :: state.pipeline }
      else if arg.(cmd) = "-typecheck" && arg_size = 2 then
        { state with pipeline = (AnalysisAst Typecheck.typecheck_prog) :: state.pipeline }
      else if arg.(cmd) = "-uniqueify-labels" && arg_size = 2 then
        { state with pipeline = (TransformAst Hacks.uniqueify_labels) :: state.pipeline }
      else if arg.(cmd) = "-replace-unknowns" && arg_size = 2 then
        { state with pipeline = (TransformAst Hacks.replace_unknowns) :: state.pipeline }
      else if arg.(cmd) = "-bberror-assume-false" && arg_size = 2 then
        { state with pipeline = (TransformAstCfg Hacks.bberror_assume_false) :: state.pipeline }
      else if arg.(cmd) = "-flatten-mem" && arg_size = 2 then
        { state with pipeline = (TransformAst Flatten_mem.flatten_mem_program) :: state.pipeline }
      else if arg.(cmd) = "-usedef" && arg_size = 2 then
        { state with pipeline = (AnalysisAstCfg Bap_analysis.usedef) :: state.pipeline }
      else if arg.(cmd) = "-defuse" && arg_size = 2 then
        { state with pipeline = (AnalysisAstCfg Bap_analysis.defuse) :: state.pipeline }
      else
        failwith (arg.(cmd) ^ "/" ^ (string_of_int arg_size) ^ " is not a recognised command")
    else
      failwith "Need to specify at least a type and a command"
  
let rec iltrans_apply_cmd prog = function
  | AnalysisAst f -> (
    match prog with
    | Ast p as p' -> f p; p'
    | _ -> failwith "need explicit translation to AST"
  )
  | AnalysisAstCfg f -> (
    match prog with
    | AstCfg p as p' -> f p; p'
    | _ -> failwith "need explicit translation to AST CFG"
  )
  | AnalysisSsa f -> (
    match prog with
    | Ssa p as p' -> f p; p'
    | _ -> failwith "need explicit translation to SSA"
  )
  | TransformAst f -> (
    match prog with
    | Ast p -> Ast(f p)
    | _ -> failwith "need explicit translation to AST"
  )
  | TransformAstCfg f -> (
    match prog with
    | AstCfg p -> AstCfg(f p)
    | _ -> failwith "need explicit translation to AST CFG"
  )
  | TransformSsa f -> (
    match prog with
    | Ssa p -> Ssa(f p)
    | _ -> failwith "need explicit translation to SSA"
  )
  | ToCfg(special_error) -> (
    match prog with
    | Ast p -> AstCfg(Cfg_ast.of_prog ~special_error p)
    | Ssa p -> AstCfg(Cfg_ssa.to_astcfg p)
    | AstCfg _ as p -> prerr_endline "Warning: null transformation"; p
  )
  | ToAst(special_error) -> (
    match prog with
    | AstCfg p -> Ast(Cfg_ast.to_prog p)
    | p -> iltrans_apply_cmd (iltrans_apply_cmd p (ToCfg special_error)) (ToAst special_error)
  )
  | ToSsa(special_error) -> (
    match prog with
    | AstCfg p -> Ssa(Cfg_ssa.of_astcfg p)
    | p -> iltrans_apply_cmd (iltrans_apply_cmd p (ToCfg special_error)) (ToSsa special_error)
  )

let apply_cmd cmd_opts prog_term =
  let () = Bap_analysis.result := [] in
  let state = List.fold_left process_arg default_state cmd_opts in
    List.fold_left iltrans_apply_cmd prog_term (List.rev state.pipeline)
