(* topredicate.ml                                                          *)
(*                                                                         *)
(* This program is free software; you can redistribute it and/or modify    *)
(* it under the terms of the GNU General Public License as published by    *)
(* the Free Software Foundation; either version 2 of the License, or (at   *)
(* your option) any later version.                                         *)
(*                                                                         *)
(* This program is distributed in the hope that it will be useful, but     *)
(* WITHOUT ANY WARRANTY; without even the implied warranty of              *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        *)
(* General Public License for more details.                                *)
(*                                                                         *)
(* You should have received a copy of the GNU General Public License       *)
(* along with this program; if not, write to the Free Software             *)
(* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA *)

open Ast
open Ast_convenience
open Grammar_scope
open Type
open Vc
open Utils_common

type state = { options: Vc.options; irout: out_channel option; post: string; stpout: out_channel option; stpoutname: string; pstpout: out_channel option option; usedc: bool; usesccvn: bool; solve: bool; timeout: int option; vc: Vc.t }

let default_state = { 
  options = Vc.default_options;
  irout = Some stdout;
  post = "true";
  stpout = None;
  stpoutname = "";
  pstpout = None;
  usedc = true;
  usesccvn = true;
  solve = false;
  timeout = None;
  vc = compute_wp_gen
}

let typ = 0
let cmd = 1
let argN n arg = arg.(2+n)

let process_arg state arg =
  let arg_size = Array.length(arg) in
    if arg_size >= 2 && arg.(typ) = "topredicate" then
      if arg.(cmd) = "-o" && arg_size = 3 then
        { state with irout = Some(open_out (argN 0 arg)) }
      else if arg.(cmd) = "-stp-out" && arg_size = 3 then
        { state with stpoutname = (argN 0 arg); stpout = Some(open_out (argN 0 arg)) }
      else if arg.(cmd) = "-pstp-out" && arg_size <= 3 then
        let oc = if arg_size = 2 then None else Some(open_out (argN 0 arg)) in
          { state with pstpout = Some(oc) }
      else if arg.(cmd) = "-q" && arg_size = 2 then
        { state with irout = None }
      else if arg.(cmd) = "-post" && arg_size = 3 then
        { state with post = argN 0 arg }
      else if arg.(cmd) = "-dwp" && arg_size = 2 then
        { state with vc = compute_dwp_gen }
      else if arg.(cmd) = "-fwp" && arg_size = 2 then
        { state with vc = compute_fwp_gen }
      else if arg.(cmd) = "-fwpuwp" && arg_size = 2 then
        { state with vc = compute_fwp_uwp_gen }
      else if arg.(cmd) = "-fwplazyconc" && arg_size = 2 then
        { state with vc = compute_fwp_lazyconc_gen }
      else if arg.(cmd) = "-fwplazyconcuwp" && arg_size = 2 then
        { state with vc = compute_fwp_lazyconc_uwp_gen }
      else if arg.(cmd) = "-dwpk" && arg_size = 3 then
        { state with vc = compute_dwp_gen; options = { state.options with k = int_of_string(argN 0 arg) } }
      else if arg.(cmd) = "-fwplazyconcuwp" && arg_size = 2 then
        { state with vc = compute_fwp_lazyconc_uwp_gen }
      else if arg.(cmd) = "-dwp1" && arg_size = 2 then
        { state with vc = compute_dwp1_gen }
      else if arg.(cmd) = "-flanagansaxe" && arg_size = 2 then
        { state with vc = compute_flanagansaxe_gen }
      else if arg.(cmd) = "-wp" && arg_size = 2 then
        { state with vc = compute_wp_gen }
      else if arg.(cmd) = "-pwp" && arg_size = 2 then
        { state with vc = compute_passified_wp_gen }
      else if arg.(cmd) = "-uwp" && arg_size = 2 then
        { state with vc = compute_uwp_gen }
      else if arg.(cmd) = "-uwpe" && arg_size = 2 then
        { state with vc = compute_uwp_efficient_gen }
      else if arg.(cmd) = "-fse-bfs" && arg_size = 2 then
        { state with vc = compute_fse_bfs_gen }
      else if arg.(cmd) = "-fse-bfs-maxdepth" && arg_size = 3 then
        { state with vc = compute_fse_bfs_maxdepth_gen (int_of_string(argN 0 arg)) }
      else if arg.(cmd) = "-fse-maxrepeat" && arg_size = 3 then
        { state with vc = compute_fse_maxrepeat_gen (int_of_string(argN 0 arg)) }
      else if arg.(cmd) = "-solver" && arg_size = 3 then
        (Solver.set_solver (argN 0 arg); state)
      else if arg.(cmd) = "-noopt" && arg_size = 2 then
        { state with usedc = false; usesccvn = false }
      else if arg.(cmd) = "-opt" && arg_size = 2 then
        { state with usedc = true; usesccvn = true }
      else if arg.(cmd) = "-optdc" && arg_size = 2 then
        { state with usedc = true; usesccvn = false }
      else if arg.(cmd) = "-optsccvn" && arg_size = 2 then
        { state with usedc = false; usesccvn = true }
      else if arg.(cmd) = "-solve" && arg_size = 2 then
        { state with solve = true }
      else if arg.(cmd) = "-solvetimeout" && arg_size = 3 then
        { state with timeout = Some(int_of_string(argN 0 arg)) }
      else if arg.(cmd) = "-validity" && arg_size = 2 then
        { state with options = { state.options with mode = Type.Validity } }
      else if arg.(cmd) = "-sat" && arg_size = 2 then
        { state with options = { state.options with mode = Type.Sat } }
      else
        failwith (arg.(cmd) ^ " is not a recognised command")
    else
      failwith "Need to specify at least a type and a command"

let m2a_state = Memory2array.create_state()

let apply_cmd args ast_term =
  let state = List.fold_left process_arg default_state args in
  let state = 
    if state.solve && BatOption.is_none state.stpout 
    then
      let temp_filename = Filename.temp_file "bap_" ".stp" in
      let () = print_endline ("No stpout option specified - using " ^ temp_filename ^ " to store STP formula") in
        { state with stpout = Some(open_out temp_filename); stpoutname = temp_filename } 
    else 
      state 
  in
  let scope = Grammar_private_scope.default_scope() in
  let post_expr, scope = Parser.exp_from_string ~scope state.post in
  let post_type = Typecheck.infer_ast post_expr in
  let () = match post_type with | Type.Reg 1 -> () | _ -> failwith "Post condition should be a boolean type" in
  let post_expr = Memory2array.coerce_exp_state ~scope m2a_state post_expr in
  let cfg = Prune_unreachable.prune_unreachable_ast(Cfg_ast.of_prog(Memory2array.coerce_prog_state ~scope m2a_state ast_term)) in
  let (wp, foralls) =
    if state.usedc || state.usesccvn then
      let () = print_endline "Applying optimizations..." in
      let { Cfg_ssa.cfg=ssacfg; to_ssavar=tossa } = Cfg_ssa.trans_cfg cfg in
      let () = Pp.output_varnums := true in
      let post_expr = rename_astexp tossa post_expr in
      let freevars = Formulap.freevars post_expr in
      let ssacfg = Ssa_simp.simp_cfg ~liveout:freevars ~usedc:state.usedc ~usesccvn:state.usesccvn ssacfg in
      let () = print_endline "Computing predicate..." in
        vc_ssacfg state.vc state.options ssacfg post_expr
    else
      let () = print_endline "Computing predicate..." in
        vc_astcfg state.vc state.options cfg post_expr
  in
    (match state.irout with
      | None -> ()
      | Some oc ->
        let () = print_endline "Printing predicate as BAP expression" in
        let p = new Pp.pp_oc oc in
        let () = p#ast_exp wp in
          p#close
    );
    (match state.pstpout with
      | None -> ()
      | Some oc_opt ->
        let () = print_endline "Printing predicate as SMT formula" in
        let foralls = List.map (Memory2array.coerce_rvar_state m2a_state) foralls in 
        let pp = (((!Solver.solver)#printer) :> Formulap.fppf) in
          match oc_opt with
          | None -> 
              let p = pp stdout in
                p#forall foralls;
                p#ast_exp wp
          | Some oc ->
              let p = pp oc in
                p#forall foralls;
                p#ast_exp wp;
                p#close
   );
    (match state.stpout with
      | None -> 
        (ast_term, None, Some wp, Some foralls)
      | Some oc ->
        let () = print_endline "Printing predicate as SMT formula" in
        let foralls = List.map (Memory2array.coerce_rvar_state ~scope m2a_state) foralls in 
        let pp = (((!Solver.solver)#printer) :> Formulap.fppf) in
        let p = pp oc in
          (match state.options with
            | {mode=Sat} ->
              p#assert_ast_exp ~foralls wp
            | {mode=Validity} ->
              p#valid_ast_exp ~foralls wp
            | {mode=Foralls} ->
              failwith "Foralls formula mode unsupported at this level"
          );
          p#counterexample;
          p#close;
          if state.solve then (
            print_endline "Solving"
            ; let r = (!Solver.solver)#solve_formula_file ?timeout:state.timeout ~getmodel:true state.stpoutname in
                (print_endline ("Solve result: " ^ (Smtexec.result_to_string r))
                 ; match r with 
                   | Smtexec.SmtError _ -> 
                      failwith "Solver error" 
                   | Smtexec.Invalid model -> 
                      (ast_term, Some model, Some wp, Some foralls)
                   | _ -> 
                      (ast_term, None, Some wp, Some foralls)
                )
          ) else (
            (ast_term, None, Some wp, Some foralls)
          )
    )
