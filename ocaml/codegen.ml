(* codegen.ml                                                              *)
(*                                                                         *)
(* This program is free software; you can redistribute it and/or modify    *)
(* it under the terms of the GNU General Public License as published by    *)
(* the Free Software Foundation; either version 2 of the License, or (at   *)
(* your option) any later version.                                         *)
(*                                                                         *)
(* This program is distributed in the hope that it will be useful, but     *)
(* WITHOUT ANY WARRANTY; without even the implied warranty of              *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        *)
(* General Public License for more details.                                *)
(*                                                                         *)
(* You should have received a copy of the GNU General Public License       *)
(* along with this program; if not, write to the Free Software             *)
(* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA *)

open Ast
open Big_int_convenience
open Llvm_codegen

type state = { out: out_channel option; exec: bool; memimpl: memimpl }

let default_state = { out = None; exec = false; memimpl = FuncMulti }

let typ = 0
let cmd = 1
let argN n arg = arg.(2+n)

let process_arg state arg =
  if Array.length(arg) >= 2 && arg.(typ) = "codegen" then
    if arg.(cmd) = "-o" then
      { state with out = Some(open_out (argN 0 arg)) }
    else if arg.(cmd) = "-memimpl" then
      { state with memimpl = BatOption.default (state.memimpl) (string_to_memimpl (argN 0 arg)) }
    else
      failwith (arg.(cmd) ^ " is not a recognised command")
  else
    failwith "Need to specify at least a type and a command"

let apply_cmd args astcfg_term =
  let astcfg_term = Prune_unreachable.prune_unreachable_ast astcfg_term in
  let state = List.fold_left process_arg default_state args in
  let codegen = new codegen state.memimpl in
  let _ = codegen#convert_cfg astcfg_term in
    match state.out with
      | Some o -> 
          if (codegen#output_bitcode o) then
            astcfg_term
          else
            failwith "Unable to write bitcode to file"
      | None ->
          astcfg_term
          