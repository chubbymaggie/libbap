#!/usr/bin/env python
#
# vc.py
# Copyright (C) 2013 Carl Pulley <c.j.pulley@hud.ac.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details. 
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 

#---------------------------------------------------------------
# Verification Condition (weakest precondition - WP) Commands
#---------------------------------------------------------------

# Functions for computing the weakest preconditions (WPs) of programs.
#
# Given a program [p] and a postcondition [q], the weakest
# precondition [wp(p,q)] describes all input states that will cause
# [p] to terminate in a state satisfying [q]. Generally, the
# precondition is solved with an SMT solver.

def dwp():
  """
  Use efficient directionless weakest precondition.

  dwp is the same as dwp1, except that it generates a precondition that does 
  not need quantifiers.
  """
  return [["topredicate", "-dwp"]]

def fwp():
  "Use efficient directionless weakest precondition algorithm"
  return [["topredicate", "-fwp"]]

def fwpuwp():
  "Use efficient forward weakest precondition algorithm in UWP mode"
  return [["topredicate", "-fwpuwp"]]

def fwplazyconc():
  "Use efficient forward weakest precondition algorithm with concrete evaluation and lazy merging"
  return [["topredicate", "-fwplazyconc"]]

def fwplazyconcuwp():
  "Use efficient forward weakest precondition algorithm with concrete evaluation and lazy merging directly on a CFG representation"
  return [["topredicate", "-fwplazyconcuwp"]]

def dwpk(num):
  "Use efficient directionless weakest precondition (expressions larger than <num> will be given their own temporary variable assignment in the formula)"
  return [["topredicate", "-dwpk", str(num)]]

def dwp1():
  "Use 1st order efficient directionless weakest precondition"
  return [["topredicate", "-dwp1"]]

def flanagansaxe():
  "Use Flanagan and Saxe's algorithm instead of the default WP."
  return [["topredicate", "-flanagansaxe"]]

def wp():
  """
  Use Dijkstra's classic WP algorithm to compute WP, applying simp to 
  simplify each intermediate expression during the calculation.  See 
  "A Discipline of Programming" by Dijkstra.  

  This algorithm may produce formulas exponential in the size of the
  program.
  """
  return [["topredicate", "-wp"]]

def pwp():
  """
  Efficient WP on passified GCL programs.

  Similar to wp, but is intended for passified programs. Unlike wp, it does 
  not duplicate the post-condition.
  """
  return [["topredicate", "-pwp"]]

def uwp():
  """
  Use WP for Unstructured Programs.

  See "Weakest-Precondition of Unstructured Programs" by Barnett for the 
  general technique.
  """
  return [["topredicate", "-uwp"]]

def uwpe():
  "Use efficient WP for Unstructured Programs"
  return [["topredicate", "-uwpe"]]
